import 'package:flutter/material.dart';
import 'package:flutter_sparkline/flutter_sparkline.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';


class ArticlePage extends StatefulWidget {
  @override
  _ArticlePageState createState() => _ArticlePageState();
}

class _ArticlePageState extends State<ArticlePage>{

  final List<List<double>> charts =
  [
    [0.0, 0.3, 0.7, 0.6, 0.55, 0.8, 1.2, 1.3, 1.35, 0.9, 1.5, 1.7, 1.8, 1.7, 1.2, 0.8, 1.9, 2.0, 2.2, 1.9, 2.2, 2.1, 2.0, 2.3, 2.4, 2.45, 2.6, 3.6, 2.6, 2.7, 2.9, 2.8, 3.4],
    [0.0, 0.3, 0.7, 0.6, 0.55, 0.8, 1.2, 1.3, 1.35, 0.9, 1.5, 1.7, 1.8, 1.7, 1.2, 0.8, 1.9, 2.0, 2.2, 1.9, 2.2, 2.1, 2.0, 2.3, 2.4, 2.45, 2.6, 3.6, 2.6, 2.7, 2.9, 2.8, 3.4, 0.0, 0.3, 0.7, 0.6, 0.55, 0.8, 1.2, 1.3, 1.35, 0.9, 1.5, 1.7, 1.8, 1.7, 1.2, 0.8, 1.9, 2.0, 2.2, 1.9, 2.2, 2.1, 2.0, 2.3, 2.4, 2.45, 2.6, 3.6, 2.6, 2.7, 2.9, 2.8, 3.4,],
    [0.0, 0.3, 0.7, 0.6, 0.55, 0.8, 1.2, 1.3, 1.35, 0.9, 1.5, 1.7, 1.8, 1.7, 1.2, 0.8, 1.9, 2.0, 2.2, 1.9, 2.2, 2.1, 2.0, 2.3, 2.4, 2.45, 2.6, 3.6, 2.6, 2.7, 2.9, 2.8, 3.4, 0.0, 0.3, 0.7, 0.6, 0.55, 0.8, 1.2, 1.3, 1.35, 0.9, 1.5, 1.7, 1.8, 1.7, 1.2, 0.8, 1.9, 2.0, 2.2, 1.9, 2.2, 2.1, 2.0, 2.3, 2.4, 2.45, 2.6, 3.6, 2.6, 2.7, 2.9, 2.8, 3.4, 0.0, 0.3, 0.7, 0.6, 0.55, 0.8, 1.2, 1.3, 1.35, 0.9, 1.5, 1.7, 1.8, 1.7, 1.2, 0.8, 1.9, 2.0, 2.2, 1.9, 2.2, 2.1, 2.0, 2.3, 2.4, 2.45, 2.6, 3.6, 2.6, 2.7, 2.9, 2.8, 3.4]
  ];

  static final List<String> chartDropdownItems = [ 'Last 7 days', 'Last month', 'Last year' ];
  String actualDropdown = chartDropdownItems[0];
  int actualChart = 0;

  @override
  Widget build(BuildContext context)
  {
    return Scaffold
      (
        body: StaggeredGridView.count(
          crossAxisCount: 2,
          crossAxisSpacing: 12.0,
          mainAxisSpacing: 12.0,
          padding: EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
          children: <Widget>[
            _buildTile(
              Column
                  (
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>
                    [
                      Material
                        (
                          color: Colors.cyan,
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(Radius.circular(4.0))
                          ),
                          child: Padding
                            (
                            padding: const EdgeInsets.all(46.0),
                            child: Text('Image',textAlign: TextAlign.center,)
                          )
                      ),
                      Padding(padding: EdgeInsets.only(bottom: 16.0)),
                      Padding(
                          padding: EdgeInsets.all(10.0),
                          child:Text('Diabetes in the world', style: TextStyle(color: Colors.black, fontWeight: FontWeight.w700, fontSize: 24.0)),
                      ),
                      Padding(
                          padding: EdgeInsets.all(10.0),
                          child: Text('Writen by Alejandro Gonzales', style: TextStyle(color: Colors.black45)),
                      ),
                      SizedBox(
                        height: 25.0,
                      ),
                      RichText(
                        textAlign: TextAlign.right,
                        text: TextSpan(
                          text: 'Rssead More ',
                          style: TextStyle(color: Colors.blueAccent)
                        ),
                      )
                    ]
                ),
            ),
            _buildTile(
              Column
                (
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>
                  [
                    Material
                      (
                        color: Colors.pink,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(Radius.circular(4.0))
                        ),
                        child: Padding
                          (
                            padding: const EdgeInsets.all(46.0),
                            child: Text('Image',textAlign: TextAlign.center,)
                        )
                    ),
                    Padding(padding: EdgeInsets.only(bottom: 16.0)),
                    Padding(
                      padding: EdgeInsets.all(10.0),
                      child:Text('Why fruits are good for you?', style: TextStyle(color: Colors.black, fontWeight: FontWeight.w700, fontSize: 24.0)),
                    ),
                    Padding(
                      padding: EdgeInsets.all(10.0),
                      child: Text('Writen by John Doe', style: TextStyle(color: Colors.black45)),
                    ),
                    SizedBox(
                      height: 25.0,
                    ),
                    RichText(
                      textAlign: TextAlign.end,
                      text: TextSpan(
                          text: 'Read More',
                          style: TextStyle(color: Colors.blueAccent)
                      ),
                    )
                  ]
              ),
            ),
            _buildTile(
              Column
                (
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>
                  [
                    Material
                      (
                        color: Colors.deepPurpleAccent,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(Radius.circular(4.0))
                        ),
                        child: Padding
                          (
                            padding: const EdgeInsets.all(46.0),
                            child: Text('Image',textAlign: TextAlign.center,)
                        )
                    ),
                    Padding(padding: EdgeInsets.only(bottom: 16.0)),
                    Padding(
                      padding: EdgeInsets.all(10.0),
                      child:Text('Why fruits are good for you again?', style: TextStyle(color: Colors.black, fontWeight: FontWeight.w700, fontSize: 24.0)),
                    ),
                    Padding(
                      padding: EdgeInsets.all(10.0),
                      child: Text('Writen by John Doe', style: TextStyle(color: Colors.black45)),
                    ),
                    SizedBox(
                      height: 25.0,
                    ),
                    RichText(
                      textAlign: TextAlign.end,
                      text: TextSpan(
                          text: 'Read More',
                          style: TextStyle(color: Colors.blueAccent)
                      ),
                    )
                  ]
              ),
            ),
            _buildTile(
              Column
                (
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>
                  [
                    Material
                      (
                        color: Colors.amber,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(Radius.circular(4.0))
                        ),
                        child: Padding
                          (
                            padding: const EdgeInsets.all(46.0),
                            child: Text('Image',textAlign: TextAlign.center,)
                        )
                    ),
                    Padding(padding: EdgeInsets.only(bottom: 16.0)),
                    Padding(
                      padding: EdgeInsets.all(10.0),
                      child:Text('Why polar bears are dying because of your diet', style: TextStyle(color: Colors.black, fontWeight: FontWeight.w700, fontSize: 24.0)),
                    ),
                    Padding(
                      padding: EdgeInsets.all(10.0),
                      child: Text('Writen by Jalilo & Stitch', style: TextStyle(color: Colors.black45)),
                    ),
                    SizedBox(
                      height: 25.0,
                    ),
                    RichText(
                      textAlign: TextAlign.end,
                      text: TextSpan(
                          text: 'Read More',
                          style: TextStyle(color: Colors.blueAccent)
                      ),
                    )
                  ]
              ),
            ),
            _buildTile(
              Column
                (
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>
                  [
                    Material
                      (
                        color: Colors.green,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.all(Radius.circular(4.0))
                        ),
                        child: Padding
                          (
                            padding: const EdgeInsets.all(46.0),
                            child: Text('Image',textAlign: TextAlign.center,)
                        )
                    ),
                    Padding(padding: EdgeInsets.only(bottom: 16.0)),
                    Padding(
                      padding: EdgeInsets.all(10.0),
                      child:Text('Better health in 10 steps', style: TextStyle(color: Colors.black, fontWeight: FontWeight.w700, fontSize: 24.0)),
                    ),
                    Padding(
                      padding: EdgeInsets.all(10.0),
                      child: Text('Writen by Meriem Tazen', style: TextStyle(color: Colors.black45)),
                    ),
                    SizedBox(
                      height: 25.0,
                    ),
                    RichText(
                      textAlign: TextAlign.end,
                      text: TextSpan(
                          text: 'Read More',
                          style: TextStyle(color: Colors.blueAccent)
                      ),
                    )
                  ]
              ),
            ),
            _buildTile(
              Padding
                (
                  padding: const EdgeInsets.all(24.0),
                  child: Column
                    (
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>
                    [
                      Row
                        (
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>
                        [
                          Column
                            (
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>
                            [
                              Text('Deaths by diabetes', style: TextStyle(color: Colors.green)),
                              Text('16K', style: TextStyle(color: Colors.black, fontWeight: FontWeight.w700, fontSize: 34.0)),
                            ],
                          ),
                          DropdownButton
                            (
                              isDense: true,
                              value: actualDropdown,
                              onChanged: (String value) => setState(()
                              {
                                actualDropdown = value;
                                actualChart = chartDropdownItems.indexOf(value); // Refresh the chart
                              }),
                              items: chartDropdownItems.map((String title)
                              {
                                return DropdownMenuItem
                                  (
                                  value: title,
                                  child: Text(title, style: TextStyle(color: Colors.blue, fontWeight: FontWeight.w400, fontSize: 14.0)),
                                );
                              }).toList()
                          )
                        ],
                      ),
                      Padding(padding: EdgeInsets.only(bottom: 4.0)),
                      Sparkline
                        (
                        data: charts[actualChart],
                        lineWidth: 5.0,
                        lineColor: Colors.greenAccent,
                      )
                    ],
                  )
              ),
            ),
          ],
          staggeredTiles: [
            StaggeredTile.extent(1, 325.0),
            StaggeredTile.extent(1, 300.0),
            StaggeredTile.extent(1, 330.0),
            StaggeredTile.extent(1, 365.0),
            StaggeredTile.extent(1, 300.0),
            StaggeredTile.extent(2, 220.0),
          ],
        )
    );
  }

  Widget _buildTile(Widget child, {Function() onTap}) {
    return Material(
        elevation: 14.0,
        borderRadius: BorderRadius.circular(12.0),
        shadowColor: Color(0xffEEEEEE),
        child: InkWell
          (
          // Do onTap() if it isn't null, otherwise do print()
            onTap: onTap != null ? () => onTap() : () { print('Not set yet'); },
            child: child
        )
    );
  }
}

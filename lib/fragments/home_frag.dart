import 'package:flutter/material.dart';

class HomeFragment extends StatefulWidget {

   Map<String, dynamic> identity = {};
  HomeFragment(this.identity);

  @override
  State<StatefulWidget> createState() => _HomePageState();
}

class _HomePageState extends State<HomeFragment> {
  @override
  Widget build(BuildContext context) {
    Map<String, dynamic> _identity = widget.identity;
    return Container(
      color: Colors.white,
      child: Center(
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Container(
                margin: EdgeInsets.all(8.0),
                child: Text(
                  'Welcome to your Health App',
                  style: TextStyle(
                    fontSize: 22.0,
                    fontWeight: FontWeight.bold,
                    fontFamily: "SegoeUI",
                    color: Color(0xff362642),
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.all(5.0),
                child: Text(
                  'Tracking your health, made easier !',
                  style: TextStyle(
                    fontSize: 17.0,
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.symmetric(
                  horizontal: 100.0,
                  vertical: 10.0,
                ),
                height: 1.2,
                color: Colors.blueGrey,
              ),
              Container(
                margin: EdgeInsets.symmetric(
                  horizontal: 30.0,
                  vertical: 5.0,
                ),
                child: Text(
                  'Follow your evolutions daily through charts and find personalised articles directly related to your personnal health. New features will be out every months !',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontSize: 17.0,
                  ),
                ),
              ),
              SizedBox(height: 10.0),
              this._buttons('Weight', true, context),
              this._buttons('BMI', true, context),
              this._buttons('Steps', false, context),
            ],
          ),
        ),
      ),
    );
  }

  _buttons(String name, bool isActive, BuildContext context) {
    Icon _leftIcon;
    if (isActive) {
      _leftIcon = Icon(
        Icons.equalizer,
        color: Color(0xff73A1CF),
      );
    } else {
      _leftIcon = Icon(
        Icons.brightness_1,
        color: Color(0xffDDDDDD),
      );
    }

    return GestureDetector(
      onTap: () {
        _action(name, context);
      },
      child: Container(
        margin: EdgeInsets.only(top: 5.0),
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5.0),
            border: Border.all(color: Color(0xffEEEEEE)),
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Color(0xffEEEEEE),
                offset: Offset(3.0, 4.0),
                spreadRadius: 1.0,
                blurRadius: 4.0,
              )
            ],
          ),
          margin: EdgeInsets.symmetric(
            horizontal: 30.0,
            vertical: 5.0,
          ),
          height: 80.0,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(left: 40.0),
                child: Row(
                  children: <Widget>[
                    _leftIcon,
                    SizedBox(width: 25.0),
                    Text(
                      name,
                      style: TextStyle(
                        color: Color(0xff336699),
                      ),
                    ),
                  ],
                ),
              ),
              Container(
                margin: EdgeInsets.only(right: 40.0),
                child: Icon(
                  Icons.chevron_right,
                  color: Colors.blueGrey,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  _action(String name, BuildContext context) {
    //steps
    if (name.toLowerCase() == "steps") {
      return showDialog(
        context: context,
        builder: (BuildContext c) {
          return AlertDialog(
            content: Container(
              height: 150,
              child: Column(
                children: <Widget>[
                  Text(
                    'Steps tracking will be added soon !',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Color(0xff336699),
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Text(
                    'New Features are added every month,',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Color(0xff336699),
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  Text(
                    'Stay tunned !',
                    style: TextStyle(
                      color: Color(0xff336699),
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              ),
            ),
            actions: <Widget>[
              FlatButton(
                child: Text(
                  'GOT IT',
                  style: TextStyle(
                    color: Color(0xff336699),
                    fontWeight: FontWeight.bold,
                  ),
                ),
                onPressed: () {
                  Navigator.pop(context);
                },
              )
            ],
          );
        },
      );
    } else if (name.toLowerCase() == 'bmi') {
      return Navigator.pushNamed(context, '/bmi');
    } else if (name.toLowerCase() == 'weight') {
      return Navigator.pushNamed(context, '/weight');
    }
  }
}

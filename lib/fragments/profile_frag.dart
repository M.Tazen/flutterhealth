import 'package:flutter/material.dart';

class ProfileFragment extends StatefulWidget {
  Map<String, dynamic> identity = {};
  ProfileFragment(this.identity);

  @override
  State<StatefulWidget> createState() {
    return _ProfileFragmentState();
  }
}

class _ProfileFragmentState extends State<ProfileFragment> {
  @override
  Widget build(BuildContext context) {
    Map<String, dynamic> _identity = widget.identity;

    return Container(
      color: Colors.white,
      child: Center(
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Container(
                margin: EdgeInsets.fromLTRB(0, 20, 0, 8.0),
                child: Text(
                  'My connected clothes',
                  style: TextStyle(
                      fontSize: 20.0,
                      fontWeight: FontWeight.bold,
                      fontFamily: "SegoeUI"),
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  GestureDetector(
                    onTap: () {
                      _action('clothedetails', context);
                    },
                    child: Container(
                      margin: EdgeInsets.only(top: 5.0),
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(5.0),
                          border: Border.all(color: Color(0xffEEEEEE)),
                          color: Colors.white,
                          boxShadow: [
                            BoxShadow(
                              color: Color(0xffEEEEEE),
                              offset: Offset(3.0, 4.0),
                              spreadRadius: 0.5,
                              blurRadius: 6.0,
                            )
                          ],
                        ),
                        margin: EdgeInsets.symmetric(
                          horizontal: 5.0,
                          vertical: 5.0,
                        ),
                        height: 120,
                        width: 120,
                        child: new AspectRatio(
                          aspectRatio: 487 / 451,
                          child: new Container(
                            decoration: new BoxDecoration(
                                image: new DecorationImage(
                              fit: BoxFit.fitWidth,
                              alignment: FractionalOffset.topCenter,
                              image: new NetworkImage(
                                  'https://cdn2.bigcommerce.com/n-d57o0b/jtyld/products/91/images/483/2_Socks_23_view_no_ANKLET__68817.1446500821.560.560.jpg?c=2'),
                            )),
                          ),
                        ),
                      ),
                    ),
                  ),
                  this._buttons('add', context),
                ],
              ),
              Container(
                margin: EdgeInsets.fromLTRB(0, 60.0, 0, 8),
                child: Text(
                  'My personnal informations',
                  style: TextStyle(
                      fontSize: 20.0,
                      fontWeight: FontWeight.bold,
                      fontFamily: "SegoeUI"),
                ),
              ),
              Container(
                  margin: EdgeInsets.symmetric(horizontal: 5.0, vertical: 5.0),
                  child: Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(5.0),
                        border: Border.all(color: Color(0xffEEEEEE)),
                        color: Colors.white,
                        boxShadow: [
                          BoxShadow(
                            color: Color(0xffEEEEEE),
                            offset: Offset(2.0, 3.0),
                            spreadRadius: 0.5,
                            blurRadius: 6.0,
                          )
                        ],
                      ),
                      margin: EdgeInsets.symmetric(
                        horizontal: 5.0,
                        vertical: 5.0,
                      ),
                      padding: EdgeInsets.fromLTRB(15, 5, 15, 15),
                      child: Column(
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              Icon(
                                Icons.brightness_1,
                                color: Color(0xffDDDDDD),
                                size: 100,
                              ),
                              Container(
                                  margin: EdgeInsets.only(left: 15),
                                  child: Column(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceEvenly,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Text(_identity["fullname"],
                                          style: TextStyle(
                                              fontSize: 17,
                                              fontWeight: FontWeight.bold)),
                                      Text(_identity["email"],
                                          style: TextStyle(
                                              fontSize: 17,
                                              fontWeight: FontWeight.w100)),
                                    ],
                                  ))
                            ],
                          ),
                          Container(
                            margin: EdgeInsets.symmetric(
                              vertical: 10.0,
                            ),
                            height: 1.2,
                            color: Colors.blueGrey,
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  RichText(
                                    text: TextSpan(
                                      style: DefaultTextStyle.of(context).style,
                                      children: <TextSpan>[
                                        TextSpan(
                                          text: 'Birthday : ',
                                          style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 17,
                                          ),
                                        ),
                                        TextSpan(
                                          text: _identity["birthday"],
                                          style: TextStyle(
                                            fontSize: 17,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  RichText(
                                    text: TextSpan(
                                      style: DefaultTextStyle.of(context).style,
                                      children: <TextSpan>[
                                        TextSpan(
                                          text: 'Gender : ',
                                          style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 17,
                                          ),
                                        ),
                                        TextSpan(
                                          text: _identity["gender"] ==
                                                  "Gender.female"
                                              ? 'female'
                                              : 'male',
                                          style: TextStyle(
                                            fontSize: 17,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                  RichText(
                                    text: TextSpan(
                                      style: DefaultTextStyle.of(context).style,
                                      children: <TextSpan>[
                                        TextSpan(
                                          text: 'Height : ',
                                          style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 17,
                                          ),
                                        ),
                                        TextSpan(
                                          text: "${_identity["height"].toString()} cm",
                                          style: TextStyle(
                                            fontSize: 17,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ],
                      )))
            ],
          ),
        ),
      ),
    );
  }

  _buttons(String name, BuildContext context) {
    return GestureDetector(
      onTap: () {
        _action(name, context);
      },
      child: Container(
        margin: EdgeInsets.only(top: 5.0),
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(5.0),
            border: Border.all(color: Color(0xffEEEEEE)),
            color: Colors.white,
            boxShadow: [
              BoxShadow(
                color: Color(0xffEEEEEE),
                offset: Offset(3.0, 4.0),
                spreadRadius: 0.5,
                blurRadius: 6,
              )
            ],
          ),
          margin: EdgeInsets.symmetric(
            horizontal: 5.0,
            vertical: 5.0,
          ),
          height: 120,
          width: 120,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[IconButton(icon: Icon(Icons.add))],
          ),
        ),
      ),
    );
  }

  _action(String name, BuildContext context) {
    //steps
    if (name.toLowerCase() == "add") {
      return showDialog(
        context: context,
        builder: (BuildContext c) {
          return AlertDialog(
            content: Container(
              padding: EdgeInsets.all(0),
              margin: EdgeInsets.all(0),
              height: 150,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Icon(
                          Icons.bluetooth,
                          size: 15,
                        ),
                        Text(' Bluetooth')
                      ]),
                  
                  Text(
                    'Coming Soon !',
                    style: TextStyle(
                      color: Color(0xff336699),
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ],
              ),
            ),
            actions: <Widget>[
              FlatButton(
                child: Text(
                  'GOT IT',
                  style: TextStyle(
                    color: Color(0xff336699),
                    fontWeight: FontWeight.bold,
                  ),
                ),
                onPressed: () {
                  Navigator.pop(context);
                },
              )
            ],
          );
        },
      );
    } else if (name.toLowerCase() == 'clothedetails') {
      return Navigator.pushNamed(context, '/clothedetails');
    }
  }
}

import 'package:flutter/material.dart';

import 'pages/login.dart';
import 'pages/create_account.dart';
import 'pages/home.dart';
import 'pages/bmi.dart';
import 'pages/weight.dart';
import 'pages/clothe_details.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // String _email = '';
  // String _password ='';
  // String _fullname;
  // String _birthdate;
  // String _confirmPassword;
  // String _gender;
  // String _height;
  // String _weight;
  Map<String, dynamic> identity = {};

  _newIdentity(
    String email,
    // String password,
    String fullname,
    String birthday,
    String gender,
    int height,
    String weight,
  ) {
    identity['email'] = email == null ?  'marjory@delaqua.com' : email;
    // identity['password'] = password;
    identity['fullname'] = fullname == null ?  'Marjory Delaqua' : fullname;
    identity['birthday'] = birthday == null ?  '02/05/1994' : birthday;
    identity['gender'] = gender == null ?  'female' : gender;
    identity['height'] = height == null ?  '170' : height;
    identity['weight'] = weight == null ?  '55.2' : weight;
  }

  _loggedIdentity(
    String email,
    String password,
  ) {
    identity['email'] = email == null ?  'marjory@delaqua.com' : email;
    identity['password'] =  password == null ? 'test' : password;
    identity['fullname'] = "Marjory Delaqua";
    identity['birthday'] = "02/05/1994";
    identity['gender'] = "female";
    identity['height'] = 170;
    identity['weight'] = "55.2";

    print(identity);
  }

  _logout() {
    Map<String, dynamic> identity = {};
  }

  // final MaterialColor myPurple = const MaterialColor(
  //   0xFF336699,
  //   const {
  //     50: const Color(0xFF336699),
  //   },
  // );

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
          primarySwatch: Colors.deepPurple,
          accentColor: Colors.deepPurple[100],
          fontFamily: "SegoeUI"),
      routes: {
        "/": (BuildContext context) => LoginPage(_loggedIdentity),
        "/createaccount": (BuildContext context) => CreateAccountPage(_newIdentity),
        "/home": (BuildContext context) => HomePage(identity),
        "/bmi": (BuildContext context) => BmiPage(identity),
        "/weight": (BuildContext context) => WeightPage(),
        "/clothedetails": (BuildContext context) => ClotheDetails(),
      },
    );
  }
}

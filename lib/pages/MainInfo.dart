import 'package:flutter/material.dart';
import '../screenAwareSize.dart' show screenAwareSize;

class MainInfo extends StatelessWidget{
  @override

  Widget build(BuildContext context) {
    return Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Container(
                width: screenAwareSize(170.0, context),
                height: screenAwareSize(170.0, context),
                margin: EdgeInsets.all(15.0),
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    image: DecorationImage(
                        fit: BoxFit.fill,
                        image: NetworkImage(
                            "https://assets.gitlab-static.net/uploads/-/system/user/avatar/1228717/avatar.png")
                    )
                )),
            Text("John Doe",
                textScaleFactor: 1.5),
            Column(
              children: <Widget>[
                new Row(
                    children: <Widget> [
                      Expanded(
                          child:
                          Container(
                            child: Row(
                              children: <Widget>[
                                Icon(Icons.check_circle, size: screenAwareSize(30.0, context)),
                                Text('70kgs'),
                                SizedBox(width: screenAwareSize(200.0, context)),
                                Icon(Icons.show_chart, size: screenAwareSize(30.0, context)),
                                Icon(Icons.arrow_forward_ios, size: screenAwareSize(30.0, context))
                              ],
                            ),
                            height: screenAwareSize(70.0, context),
                            decoration: BoxDecoration(
                              color: Colors.white,
                              border: Border.all(
                                color: Colors.deepPurple[300],
                                width: screenAwareSize(1.0, context),
                              ),
                            ),
                          )
                      )
                    ]
                ),
                SizedBox(height: screenAwareSize(10.0, context)),
                new Row(
                    children: <Widget> [
                      Expanded(
                          child:
                          Container(
                            child: Row(
                              children: <Widget>[
                                Icon(Icons.check_circle, size: screenAwareSize(30.0, context)),
                                Text('-- bpm'),
                                SizedBox(width: screenAwareSize(200.0, context)),
                                Icon(Icons.show_chart, size: screenAwareSize(30.0, context)),
                                Icon(Icons.arrow_forward_ios, size: screenAwareSize(30.0, context))
                              ],
                            ),
                            height: screenAwareSize(70.0, context),
                            decoration: BoxDecoration(
                              color: Colors.white,
                              border: Border.all(
                                color: Colors.grey,
                                width: screenAwareSize(1.0, context),
                              ),
                            ),
                          )
                      )
                    ]
                ),
                SizedBox(height: screenAwareSize(10.0, context)),
                new Row(
                    children: <Widget> [
                      Expanded(
                          child:
                          Container(
                            child: Row(
                              children: <Widget>[
                                Icon(Icons.check_circle, size: screenAwareSize(30.0, context)),
                                Text('IMC'),
                                SizedBox(width: screenAwareSize(200.0, context)),
                                Icon(Icons.show_chart, size: screenAwareSize(30.0, context)),
                                Icon(Icons.arrow_forward_ios, size: screenAwareSize(30.0, context))
                              ],
                            ),
                            height: screenAwareSize(70.0, context),
                            decoration: BoxDecoration(
                              color: Colors.white,
                              border: Border.all(
                                color: Colors.grey,
                                width: screenAwareSize(1.0, context),
                              ),
                            ),
                          )
                      )
                    ]
                ),

              ],
            ),
          ],
        )

    );
  }


}
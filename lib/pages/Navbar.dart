import 'package:flutter/material.dart';
import 'MainInfo.dart';
import '../fragments/home_frag.dart';
import '../fragments/profile_frag.dart';
import '../fragments/article_frag.dart';

class Navbar extends StatefulWidget {
  Map<String, dynamic> identity = {};
  Navbar(this.identity);

  @override
  State<StatefulWidget> createState() => _NavbarState();
}

class _NavbarState extends State<Navbar> {
  int _selectedIndex = 0;
  String _title = 'Home';
  Color logoutButtonColor = Colors.deepPurple;

  @override
  Widget build(BuildContext context) {
    final _widgetOptions = [
      HomeFragment( widget.identity),
      ArticlePage(),
      ProfileFragment( widget.identity),
    ];


    return Container(
      margin: EdgeInsets.only(top: 20.0),
      child: Scaffold(
        appBar: AppBar(
          title: Text(this._title),
          actions: <Widget>[
            IconButton(
              onPressed: () {
                return Navigator.pushReplacementNamed(context, '/');
              },
              icon: Icon(Icons.exit_to_app),
              iconSize: 25,
              color: this.logoutButtonColor,
            )
          ],
        ),
        body: Center(
          child: _widgetOptions.elementAt(_selectedIndex),
        ),
        bottomNavigationBar: BottomNavigationBar(
          items: [
            BottomNavigationBarItem(
                icon: Icon(Icons.home, size: 38),
                title: Container(height: 0.0)),
            BottomNavigationBarItem(
                icon: Icon(Icons.dashboard, size: 38),
                title: Container(height: 0.0)),
            BottomNavigationBarItem(
                icon: Icon(Icons.person, size: 38),
                title: Container(height: 0.0)),
          ],
          currentIndex: _selectedIndex,
          fixedColor: Theme.of(context).primaryColor,
          onTap: _onItemTapped,
        ),
      ),
//      body: Center(
//        child: _widgetOptions.elementAt(_selectedIndex),
//      ),
//      bottomNavigationBar: BottomNavigationBar(
//        items: [
//          BottomNavigationBarItem(icon: Icon(Icons.home, size: 50), title: Container(height: 0.0)),
//          BottomNavigationBarItem(icon: Icon(Icons.dashboard, size: 50), title: Container(height: 0.0)),
//          BottomNavigationBarItem(icon: Icon(Icons.person, size: 50), title: Container(height: 0.0)),
//        ],
//        currentIndex: _selectedIndex,
//        fixedColor: Theme.of(context).primaryColor,
//        onTap: _onItemTapped,
//      ),
    );
  }

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
      if (index == 0) {
        this._title = 'Home';
        this.logoutButtonColor = Theme.of(context).primaryColor;
      } else if (index == 1) {
        this._title = 'Your Health Documentation';
        this.logoutButtonColor = Theme.of(context).primaryColor;
      } else {
        this._title = 'Your Account';
        this.logoutButtonColor = Colors.white;
      }
    });
  }
}

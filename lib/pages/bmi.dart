import 'package:flutter/material.dart';

class BmiPage extends StatefulWidget {
  Map<String, dynamic> identity = {};
  BmiPage(this.identity);

  @override
  State<StatefulWidget> createState() {
    return _BMIPageState();
  }
}

class _BMIPageState extends State<BmiPage> {
  @override
  Widget build(BuildContext context) {
    Map<String, dynamic> _identity = widget.identity;
    var height = _identity['height'];
    var idealWeight = (height - 100)* .90;

    return Scaffold(
      appBar: AppBar(
        title: Text("BMI"),
      ),
      body: SingleChildScrollView(
        child: Container(
          color: Colors.white,
          child: Column(
            children: <Widget>[
              SizedBox(
                height: 15.0,
              ),
              this._summaryArea('${idealWeight} kg', 'Your ideal weight', context),
              this._summaryArea("${_identity['weight']} kg", 'Your current weight', context),
              this._summaryArea("${_identity['height']} cm", 'Your height', context),
              Container(
                margin: EdgeInsets.symmetric(
                  horizontal: 30.0,
                  vertical: 20.0,
                ),
                child: Image.asset('assets/images/04-BMI.PNG'),
              ),
              Container(
                margin: EdgeInsets.only(top: 5.0),
                child: Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(5.0),
                    border: Border.all(color: Color(0xffEEEEEE)),
                    color: Colors.white,
                    boxShadow: [
                      BoxShadow(
                        color: Color(0xffEEEEEE),
                        offset: Offset(3.0, 4.0),
                        spreadRadius: 2.0,
                        blurRadius: 4.0,
                      )
                    ],
                  ),
                  padding: EdgeInsets.fromLTRB(30, 5, 30, 20),
                  margin: EdgeInsets.symmetric(
                    horizontal: 30.0,
                    vertical: 5.0,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.symmetric(
                          horizontal: 00.0,
                          vertical: 10.0,
                        ),
                        child: Text(
                          'What is BMI ?',
                          textAlign: TextAlign.start,
                          style: TextStyle(
                            fontSize: 17.0,
                            fontWeight: FontWeight.bold,
                            color: Color(0xff336699),
                          ),
                        ),
                      ),
                      Container(
                        child: Text(
                            'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, qui nostrud exercitation ullamco laboris nisi ut aliquip e eiusmod tempor incididunt ut labore et',
                            textAlign: TextAlign.justify,
                            style: TextStyle(
                              color: Color(0xff336699),
                            )),
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
  _summaryArea(String data, String name, BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 5.0),
      child: Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5.0),
          border: Border.all(color: Color(0xffEEEEEE)),
          color: Colors.white,
          boxShadow: [
            BoxShadow(
              color: Color(0xffEEEEEE),
              offset: Offset(3.0, 4.0),
              spreadRadius: 2.0,
              blurRadius: 4.0,
            )
          ],
        ),
        margin: EdgeInsets.symmetric(
          horizontal: 30.0,
          vertical: 5.0,
        ),
        height: 60.0,
        child: Row(
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(left: 50.0),
              child: Text(
                data,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontFamily: "SegoeUI",
                  color: Color(0xff336699),
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.only(left: 50.0),
              child: Text(name,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontFamily: "SegoeUI",
                    color: Color(0xff336699),
                  )),
            ),
          ],
        ),
      ),
    );
  }
}

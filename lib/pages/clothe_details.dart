import 'package:flutter/material.dart';

class ClotheDetails extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        child: Stack(
          children: <Widget>[
            Container(
              color: Colors.white,
              padding: EdgeInsets.all(20),
            ),
            Container(
              height: 130,
              color: Colors.deepPurple,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  IconButton(
                    icon: Icon(Icons.arrow_back),
                    color: Colors.white,
                    iconSize: 30,
                    onPressed: () => Navigator.of(context).pop(true),
                  ),
                  SizedBox(
                    width: 105,
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        'My profile',
                        style: TextStyle(
                            fontSize: 20,
                            color: Colors.white,
                            fontWeight: FontWeight.w600),
                      ),
                      Text('Connected cloth',
                          style: TextStyle(
                            fontSize: 20,
                            color: Colors.white,
                          ))
                    ],
                  )
                ],
              ),
            ),
            Positioned(
              top: 110.0,
              width: 400,
              left: 12,
              child: Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        Container(
                          color: Colors.white,
                          height: 160.0,
                          width: 160.0,
                          child: new AspectRatio(
                            aspectRatio: 487 / 451,
                            child: new Container(
                              decoration: new BoxDecoration(
                                  border: Border.all(color: Color(0xffdddddd)),
                                  image: new DecorationImage(
                                    fit: BoxFit.fitWidth,
                                    alignment: FractionalOffset.topCenter,
                                    image: new NetworkImage(
                                        'https://cdn2.bigcommerce.com/n-d57o0b/jtyld/products/91/images/483/2_Socks_23_view_no_ANKLET__68817.1446500821.560.560.jpg?c=2'),
                                  )),
                            ),
                          ),
                        ),
                        Container(
                          height: 90.0,
                          width: 160.0,
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Text('My super duper socks',
                                  style: TextStyle(fontSize: 18)),
                            ],
                          ),
                        ),
                      ],
                    ),
                    Column(
                      children: <Widget>[
                        Container(
                            padding: EdgeInsets.all(20),
                            margin: EdgeInsets.only(top: 10),
                            child: Text(
                              'Qui aliqua aliquip elit et. Do elit labore nulla amet velit est quis elit minim anim labore. Reprehenderit irure cillum anim sint veniam fugiat dolor consectetur dolor. Labore sit irure anim elit in magna sint ad proident enim cillum. Laboris do mollit consequat pariatur elit elit aliquip culpa laboris nostrud. Cupidatat ad tempor proident fugiat. Incididunt velit quis ad laboris deserunt exercitation dolor eu.',
                              style: TextStyle(
                                fontSize: 18,
                              ),
                              textAlign: TextAlign.justify,
                              softWrap: true,
                            )),
                        // Row(
                        //   crossAxisAlignment: CrossAxisAlignment.center,
                        //   mainAxisAlignment: MainAxisAlignment.spaceAround,
                        //   children: <Widget>[
                        //     IconButton(
                        //       icon: Icon(Icons.delete),
                        //       tooltip: 'Delete from my list',
                        //       iconSize: 50,
                        //     ),
                        //     IconButton(
                        //       icon: Icon(Icons.edit),
                        //       tooltip: 'Rename my cloth',
                        //       iconSize: 50,
                        //     ),
                        //   ],
                        // )
                      ],
                    ),
                  ],
                ),
              ),
            ),
            // Positioned(
            //   left: 40.0,
            //   top: 110.0,
            //   child: Container(
            //     color: Colors.red,
            //     height: 160.0,
            //     width: 160.0,
            //   ),
            // ),
            // Positioned(
            //   right: 40.0,
            //   top: 110.0,
            //   child: Container(
            //     color: Colors.blue,
            //     height: 160.0,
            //     width: 160.0,
            //     child: Column(
            //       mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            //       crossAxisAlignment: CrossAxisAlignment.center,
            //       children: <Widget>[
            //         Text('My super duper socks',
            //             style: TextStyle(fontSize: 18)),
            //         Text('My super duper socks',
            //             style: TextStyle(fontSize: 18)),
            //         Text('My super duper socks',
            //             style: TextStyle(fontSize: 18)),
            //       ],
            //     ),
            //   ),
            // ),
          ],
        ),
      ),
    );
  }
}

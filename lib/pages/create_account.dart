import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_health/gender/GenderCard.dart';
import 'package:flutter_health/gender/gender.dart';
import 'package:flutter_health/weight/WeightCard.dart';
import 'package:intl/intl.dart';
import '../screenAwareSize.dart' show screenAwareSize;

class CreateAccountPage extends StatefulWidget {
  Function _newIdentity;

  CreateAccountPage(this._newIdentity);

  @override
  State<StatefulWidget> createState() {
    return _CreateAccountPageState();
  }
}

class _CreateAccountPageState extends State<CreateAccountPage> {
  Gender gender = Gender.other;
  int height = 180;
  int weight = 70;

  TextEditingController txt = new TextEditingController();

  String _socksText = "Connect to your socks";
  String _scaleText = "Connect to your bathroom scale";

  String _fullname;
  String _email;
  String _birth;
  String _password;
  String _confirmPassword;
  bool _acceptTerms = false;
  bool _isScaleDisabled = false;
  bool _isSocksDisabled = false;

  _closeKeyboard() {
    return FocusScope.of(context).requestFocus(new FocusNode());
  }

  final controller = PageController(initialPage: 0, keepPage: true);
  _stepOneContent() {
    return Container(
      padding: EdgeInsets.all(20.0),
      child: Center(
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              TextField(
                decoration: InputDecoration(
                  labelText: 'Fullname',
                ),
                onChanged: (String value) {
                  setState(() {
                    _fullname = value;
                  });
                },
              ),
              TextField(
                
                controller: txt,
                decoration: InputDecoration(
                  labelText: 'Birthday',
                ),
                enabled: true,
                onTap: () {
                  DatePicker.showDatePicker(context,
                      showTitleActions: true,
                      minTime: DateTime(1900, 1, 1),
                      maxTime: DateTime.now(), onChanged: (date) {
                    print('change $date');
                  }, onConfirm: (date) {
                    print('confirm $date');
                    _birth = DateFormat('MM/dd/yyyy').format(date);
                    txt.text = _birth;
                  }, currentTime: DateTime.now(), locale: LocaleType.en);
                },
              ),

              // TextField(
              //   decoration: InputDecoration(
              //     labelText: 'Birthday date',
              //   ),
              //   onChanged: (String value) {
              //     setState(() {
              //       _birth = value;
              //     });
              //   },
              // ),
              TextField(
                keyboardType: TextInputType.emailAddress,
                decoration: InputDecoration(
                  labelText: 'Email',
                ),
                onChanged: (String value) {
                  setState(() {
                    _email = value;
                  });
                },
              ),
              SizedBox(height: 10.0),
              TextField(
                obscureText: true,
                decoration: InputDecoration(
                  labelText: 'Password',
                ),
                onChanged: (String value) {
                  setState(() {
                    _password = value;
                  });
                },
              ),
              SizedBox(height: 10.0),
              TextField(
                obscureText: true,
                decoration: InputDecoration(
                  labelText: 'Confirm Password',
                ),
                onChanged: (String value) {
                  setState(() {
                    _password = value;
                  });
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  _showDialogScale() {
    return showDialog(
      context: context,
      builder: (BuildContext c) {
        return AlertDialog(
          content: Container(
            height: 150,
            child: Column(
              children: <Widget>[
                Text(
                  'Your bathroom scale was connected successfully!!',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Color(0xff336699),
                      fontWeight: FontWeight.bold,
                      fontSize: 25.0),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 5.0),
                  child: Text(
                    'You can buy more of our products in the store!',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Color(0xff336699),
                      fontWeight: FontWeight.w200,
                    ),
                  ),
                )
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text(
                'GREAT',
                style: TextStyle(
                  color: Color(0xff336699),
                  fontWeight: FontWeight.bold,
                ),
              ),
              onPressed: () {
                _isScaleDisabled = true;
                // ignore: unnecessary_statements
                () => setState(() => _scaleText = "Connected Scale");
                // ignore: unnecessary_statements
                () => setState(() => _isScaleDisabled = true);
                Navigator.pop(context);
              },
            )
          ],
        );
      },
    );
  }

  _showDialogSocks() {
    return showDialog(
      context: context,
      builder: (BuildContext c) {
        return AlertDialog(
          content: Container(
            height: 150,
            child: Column(
              children: <Widget>[
                Text(
                  'Your socks were connected successfully!!',
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      color: Color(0xff336699),
                      fontWeight: FontWeight.bold,
                      fontSize: 25.0),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 5.0),
                  child: Text(
                    'You can buy more of our products in the store!',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      color: Color(0xff336699),
                      fontWeight: FontWeight.w200,
                    ),
                  ),
                )
              ],
            ),
          ),
          actions: <Widget>[
            FlatButton(
              child: Text(
                'GREAT',
                style: TextStyle(
                  color: Color(0xff336699),
                  fontWeight: FontWeight.bold,
                ),
              ),
              onPressed: () {
                _isSocksDisabled = true;
                // ignore: unnecessary_statements
                () => setState(() => _socksText = "Connected Socks");
                // ignore: unnecessary_statements
                () => setState(() => _isSocksDisabled = true);
                Navigator.pop(context);
              },
            )
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () {
              Navigator.pushReplacementNamed(context, "/");
            }),
        title: Text('Create your profile'),
      ),
      body: Container(
        child: PageView(
          controller: controller,
          children: <Widget>[
            _stepOneContent(),
            Container(
              child: Padding(
                padding: EdgeInsets.only(
                  top: screenAwareSize(15.0, context),
                  bottom: screenAwareSize(15.0, context),
                  left: screenAwareSize(20.0, context),
                  right: screenAwareSize(25.0, context),
                ),
                child: Column(
                  children: <Widget>[
                    Expanded(
                      child: Padding(
                          padding: EdgeInsets.only(
                              top: screenAwareSize(20.0, context)),
                          child: GenderCard(
                            gender: gender,
                            onChanged: (val) => setState(() => gender = val),
                          )),
                    ),
                    Expanded(
                      child: Padding(
                          padding: EdgeInsets.only(
                              top: screenAwareSize(20.0, context)),
                          child: WeightCard(
                            weight: height,
                            title: 'Your Height (cm)',
                            onChanged: (val) => setState(() => height = val),
                          )),
                    ),
                  ],
                ),
              ),
            ),
            Container(
              child: Padding(
                padding: EdgeInsets.only(
                  top: screenAwareSize(15.0, context),
                  bottom: screenAwareSize(15.0, context),
                  left: screenAwareSize(20.0, context),
                  right: screenAwareSize(25.0, context),
                ),
                child: Column(
                  children: <Widget>[
                    Expanded(
                      child: Padding(
                          padding: EdgeInsets.only(
                              top: screenAwareSize(20.0, context)),
                          child: WeightCard(
                            weight: weight,
                            title: 'Your weight (kg)',
                            onChanged: (val) => setState(() => weight = val),
                          )),
                    ),
                    SizedBox(height: screenAwareSize(20.0, context)),
                    ButtonTheme(
                      minWidth: screenAwareSize(250, context),
                      height: screenAwareSize(30.0, context),
                      child: RaisedButton(
                          color: _isScaleDisabled
                              ? Theme.of(context).accentColor.withOpacity(0.1)
                              : Theme.of(context).accentColor,
                          textColor: Theme.of(context).primaryColor,
                          child: Text(_scaleText),
                          onPressed: () {
                            // ignore: unnecessary_statements
                            _isScaleDisabled ? null : _showDialogScale();
                            // ignore: unnecessary_statements
                            () => setState(
                                () => _isScaleDisabled = !_isScaleDisabled);
                            // ignore: unnecessary_statements
                            () =>
                                setState(() => _scaleText = "Connected Scale");
                          }),
                    ),
                    SizedBox(height: screenAwareSize(20.0, context)),
                    ButtonTheme(
                      minWidth: screenAwareSize(250, context),
                      height: screenAwareSize(30.0, context),
                      child: RaisedButton(
                          color: _isSocksDisabled
                              ? Theme.of(context).accentColor.withOpacity(0.1)
                              : Theme.of(context).accentColor,
                          textColor: Theme.of(context).primaryColor,
                          child: Text(_socksText),
                          onPressed: () {
                            // ignore: unnecessary_statements
                            _isSocksDisabled ? null : _showDialogSocks();
                            // ignore: unnecessary_statements
                            () => setState(
                                () => _isSocksDisabled = !_isSocksDisabled);
                            // ignore: unnecessary_statements
                            () =>
                                setState(() => _socksText = "Connected Socks");
                          }),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
      bottomNavigationBar: ButtonTheme(
          minWidth: 300.0,
          height: 50.0,
          child: RaisedButton(
              color: Theme.of(context).accentColor,
              textColor: Theme.of(context).primaryColor,
              child: Text("Next"),
              onPressed: () {
                controller.nextPage(duration: kTabScrollDuration, curve: Curves.ease);
                if (controller.page > 1.999) {
                  widget._newIdentity(
                    this._email,
                    this._fullname,
                    this._birth,
                    this.gender.toString(),
                    this.height,
                    this.weight.toString(),
                  );
                  Navigator.pushReplacementNamed(context, '/home');
                }
              })),
    );
  }
}

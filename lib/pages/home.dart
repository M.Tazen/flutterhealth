import 'package:flutter/material.dart';
import 'Navbar.dart';

class HomePage extends StatefulWidget {
    Map<String, dynamic> identity = {};
  HomePage(this.identity);

  @override
  State<StatefulWidget> createState() {
    return _HomePageState();
  }
}
class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    Map<String, dynamic> _identity = widget.identity;

    return Scaffold(
        appBar: AppBar(
          title: Text('Home'),
        ),
        body: Container(
        ),
        bottomNavigationBar: Navbar(_identity),
    );
  }
}

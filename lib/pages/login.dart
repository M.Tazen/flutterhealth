import 'package:flutter/material.dart';

class LoginPage extends StatefulWidget {
  Function _loggedIdentity;
  
  LoginPage(this._loggedIdentity);

  @override
  State<StatefulWidget> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  String _email;
  String _password;
  // bool _acceptTerms = false;
  Function _loggedIdentity;

  _closeKeyboard() {
    return FocusScope.of(context).requestFocus(new FocusNode());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('Login'),
        ),
        body: Container(
          // decoration: BoxDecoration(
          //   image: DecorationImage(
          //     fit: BoxFit.cover,
          //     colorFilter: ColorFilter.mode(
          //       Colors.black.withOpacity(0.5),
          //       BlendMode.dstATop,
          //     ),
          //     image: AssetImage('assets/background.jpg'),
          //   ),
          // ),
          child: Container(
            padding: EdgeInsets.all(20.0),
            child : Center(
              child: SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    TextField(
                      keyboardType: TextInputType.emailAddress,
                      decoration: InputDecoration(
                        labelText: 'Email',
                      ),
                      onChanged: (String value) {
                        setState(() {
                          _email = value;
                        });
                      },
                    ),
                    SizedBox(height: 10.0),
                    TextField(
                      obscureText: true,
                      decoration: InputDecoration(
                        labelText: 'Password',
                      ),
                      onChanged: (String value) {
                        setState(() {
                          _password = value;
                        });
                      },
                    ),
                    // SwitchListTile(
                    //   title: Text('Accept terms'),
                    //   value: _acceptTerms, //inital value
                    //   activeColor: Theme.of(context).primaryColor,
                    //   onChanged: (bool value) {
                    //     setState(() {
                    //       _acceptTerms = value;
                    //     });
                    //   },
                    // ),
                    SizedBox(height: 30.0),
                    ButtonTheme(
                      minWidth: 220.0,
                      height: 40.0,
                      child: RaisedButton(
                          color: Theme.of(context).primaryColor,
                          textColor: Colors.white,
                        child: Text("LOGIN"),
                        onPressed: () {
                          _closeKeyboard();
                          widget._loggedIdentity(this._email, this._password);
                          Navigator.pushReplacementNamed(context, '/home');
                        },
                      ),
                    ),
                    SizedBox(height: 20.0),
                    ButtonTheme(
                      minWidth: 220.0,
                      height: 40.0,
                      child: RaisedButton(
                          color: Theme.of(context).accentColor,
                          textColor: Theme.of(context).primaryColor,
                          child: Text("Create your account"),
                          onPressed: () {
                            _closeKeyboard();
                            Navigator.pushReplacementNamed(
                                context, '/createaccount');
                          }),
                    ),
                    
                  ],
                ),
              ),
            ),
          ),
        ));
  }
}
